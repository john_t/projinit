use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use structopt::StructOpt;
mod create;
mod data;
mod template;
use std::fs;
use std::path::PathBuf;

#[macro_use]
extern crate serde;

/// You need to give a template, a project name, and an optional directory.
#[derive(StructOpt)]
struct Cli {
    template: String,
    name: String,
    dir: Option<std::path::PathBuf>,
    #[structopt(short = "d", long = "description")]
    description: Option<String>,
    #[structopt(short = "a", long = "author")]
    author: Option<String>,
}

#[derive(Deserialize, Serialize)]
struct Templates {
    templates: HashMap<String, template::Template>,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Gets the cli args.
    let args = Cli::from_args();

    // Get all the templates
    let mut templates: Templates = toml::from_str(include_str!("../data/templates.toml")).unwrap();

    // Load the correct one
    let template = templates.templates.get_mut(&args.template);

    match template {
        Some(s) => {
            if let Some(ref dir) = args.dir {
                fs::DirBuilder::new().recursive(true).create(dir).unwrap();
                std::env::set_current_dir(dir)?;
            }

            create::create_template(
                s,
                args.name,
                args.author.unwrap_or_else(|| "Author".to_string()),
                args.description.unwrap_or_default(),
                PathBuf::from("."),
            )?;
        }
        None => {
            panic!("\033[101mError: template {} not found", args.template);
        }
    }

    Ok(())
}

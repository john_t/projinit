use crate::template::*;
use bzip2::read::BzDecoder;
use colored::*;
use std::error::Error;
use std::io;
use std::io::{Read, Write};
use std::path::PathBuf;
use std::process;
use tar::Archive;

/// Creates a template in the directory given.
pub fn create_template(
    template: &mut Template,
    name: String,
    author: String,
    description: String,
    dir: PathBuf,
) -> Result<(), Box<dyn Error>> {
    // Get the term env.
    let term = {
        let mut ret = String::new();
        for k in std::env::args() {
            if k == "TERM" {
                ret = std::env::var(k)?;
            }
        }
        ret
    };

    println!("{}", "Getting binary data...".yellow());
    // Makes sure the data is in binary format
    if let Some(data) = template.data.as_mut() {
        template.data = Some(Data::to_bin(data.clone(), PathBuf::from("./"))?);
    }

    println!("{}", "Running pre commands...".yellow());
    // Run the pre commands
    if let Some(pre) = template.commands.pre.as_ref() {
        let out = process::Command::new("sh")
            .env("PROJINIT_NAME", &name)
            .env("PROJINIT_AUTHOR", &author)
            .env("PROJINIT_DESCRIPTION", &description)
            .env("TERM", &term)
            .env("PROJINIT_DESCRIPTION", &description)
            .arg("-c")
            .arg(pre)
            .output()?;
        io::stdout().write_all(&out.stdout)?;
        io::stderr().write_all(&out.stderr)?;
    }

    if let Some(data) = template.data.as_mut() {
        // Get the archive
        let archive = match data {
            Data::Bin(x) => x,
            _ => panic!("BINARY ARCHIVE NOT AVAILABLE"),
        };

        println!("{}", "Decompressing the archive...".yellow());
        // Un BZIP2 It
        let mut decompressor = BzDecoder::new(archive.as_slice());
        let mut archive: Vec<u8> = Vec::new();
        decompressor.read_to_end(&mut archive)?;

        // Un Tar it
        println!("{}", "Extracting the archive...".yellow());
        let mut archive = Archive::new(archive.as_slice());

        println!("{}", "Writing the archive...".yellow());
        // Write it
        archive.unpack(dir)?;
    }

    println!("Running post commands...");
    // Run the post commands
    if let Some(post) = template.commands.post.as_ref() {
        let out = process::Command::new("sh")
            .env("PROJINIT_NAME", name)
            .env("PROJINIT_AUTHOR", author)
            .env("PROJINIT_DESCRIPTION", description)
            .env("TERM", &term)
            .arg("-c")
            .arg(post)
            .output()?;
        io::stdout().write_all(&out.stdout)?;
        io::stderr().write_all(&out.stderr)?;
    }

    Ok(())
}

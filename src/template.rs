use crate::data::*;
use std::fs;
use std::io::Read;
use std::path;

/// The base struct for a template.
#[derive(Deserialize, Serialize)]
pub struct Template {
    pub name: String,
    pub description: String,
    #[serde(rename = "file")]
    pub data: Option<Data>,
    #[serde(default)]
    pub commands: Commands,
}

#[derive(Deserialize, Serialize, Default)]
pub struct Commands {
    pub pre: Option<String>,
    pub post: Option<String>,
}

#[derive(Deserialize, Serialize, Clone, Eq, PartialEq)]
#[serde(untagged)]
pub enum Data {
    File(String),
    Bin(Vec<u8>),
}

impl Data {
    /// This converts a data to bin, if it is a file. Path is the directory
    /// where the template is located.
    pub fn to_bin(s: Self, path: path::PathBuf) -> std::io::Result<Self> {
        Ok(match s {
            Self::File(string) => {
                match get_archive(&string) {
                    Some(archive) => Self::Bin(archive),
                    None => {
                        // Gets the file's directory
                        let dir = match path.is_dir() {
                            true => path,
                            false => path.parent().unwrap_or(&path).to_path_buf(),
                        };

                        // Gets the path.
                        dir.to_path_buf().push(string);

                        // Open the file.
                        let mut file = fs::File::open(dir)?;

                        // Read from it.
                        let mut buf = Vec::new();
                        file.read_to_end(&mut buf)?;

                        Self::Bin(buf)
                    }
                }
            }
            Self::Bin(_) => s,
        })
    }
}

# Projinit

A really simple project to provide a template cli designed to be easily
modifiable and forkable.

## Running

In you can just Projinit as follows:

```sh
projinit \
    -d "My awesome website" \
    -a "John Doe <johndoe@example.com>" \
    web-scss \
    awesome-website
```

## Configuring

Configuration is done in a suckless style configuration time modification
format.

In `projinit/data` you make a folder for the template (if it requires a
folder.) Take a look at the `web` example for a simple demonstration.

In `templates.toml` you add a section to understand the template.

This configures all the different templates that we are going to use to
create programs. They are defined as follows:

```toml
[templates.name]
# The name of the template
name = "MyAwesomeTemplate"

# The description of the template.
description = "This template is so cool it will blow your socks of."

# The file, a .tar.bz2, which will be extracted. This is the name of the
# folder with .tar.bz2 appended.
file = "template.tar.bz2"

# This defines the different commands that need to be run. They are given
# the environment variable $name, $description and $author to facilitate
# creating the new project.
commands = {
    # These commands are run before the archive is extracted. They are
    # ran through sh.
    pre = "echo 'Do Something'",

    # These are ran after the archive is extracted. They are ran through
    # sh
    post = "echo 'Do Something after the archive has been extracted'"
}

# Note `commands.pre` and `commands.post` can be used instead
```

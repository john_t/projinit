// Imports
use bzip2::{read::BzEncoder, Compression};
use std::error::Error;
use std::fs;
use std::io::{Read, Write};
use tar::Builder;

fn main() -> Result<(), Box<dyn Error>> {
    // Create the rust file to store the data in.
    let mut data_rs = fs::File::create("src/data.rs")?;

    // Write the start of the file.
    data_rs.write(
        b"use std::collections::HashMap;\n\
        use common_macros::hash_map;\n\
        pub fn get_archive(name: &str) -> Option<Vec<u8>> {\n\
            let hashmap: HashMap<&'static str, Vec<u8>> = hash_map!{\n\
        ",
    )?;

    // Get all the folders we need to tar.
    let folders = fs::read_dir("data/")?;

    // Iterate all the folders.
    for dir in folders {
        // Get the path
        let path = dir?.path();

        // Checks if it is a directory
        if !path.is_dir() {
            continue;
        }

        // Manipulates the path to a tar file.
        let mut tar_path = path.clone();
        tar_path.set_extension("tar.bz2");

        // Create a tar archive in memory
        let mut builder = Builder::new(Vec::new());
        println!("Archiving {:?}", path);
        builder.append_dir_all(".", &path)?;
        let tar_archive = builder.into_inner().unwrap();
        println!("Archived {:?}", path);

        // Get the tar path file name
        let tar_name = tar_path.file_name();

        // Create a new compressor
        let mut compressor = BzEncoder::new(tar_archive.as_slice(), Compression::best());

        println!("Compressing {:?}", path);
        // Get the compressed data.
        let mut compressed = Vec::new();
        compressor.read_to_end(&mut compressed)?;
        println!("Compressed {:?}", path);

        println!("Writing {:?}", path);
        // Add this template for this
        data_rs.flush()?;
        data_rs.write(
            format!(
                "\t\"{}\" => vec!{:?},\n",
                tar_name.as_ref().unwrap().to_str().unwrap(),
                compressed.as_slice(),
            )
            .as_bytes(),
        )?;
        println!("Written {:?}", path);

        println!("Writing {:?}", path);
        // Write it to a file for testing..
        fs::write(tar_name.unwrap(), compressed.as_slice())?;
        println!("Written {:?}", path);
    }

    // Write the end of the file.
    data_rs.write(
        b"
            };\n\
            for (k, v) in hashmap.into_iter() {
                if k == name {
                    return Some(v);
                }
            }
            None
        }
        ",
    )?;

    Ok(())
}
